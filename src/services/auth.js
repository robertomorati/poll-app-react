import Cookies from 'js-cookie'

export const isAuthenticated = () =>
  Cookies.get('token') ? Cookies.get('token') : null
export const getToken = () =>
  Cookies.get('token') ? Cookies.get('token') : null

export const login = token => {
  Cookies.set('token', token)
}
export const logout = () => {
  Cookies.remove('token')
}
