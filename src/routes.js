import React from 'react'
import SignUp from './pages/SignUp'
import SignIn from './pages/SignIn'
import MainMenu from './pages/Menu'
import ListPoll from './pages/ListPoll'
import EditPoll from './pages/EditPoll'
import CreatePoll from './pages/CreatePoll'
import ListVotesPoll from './pages/ListVotesPoll'
import VotePoll from './pages/VotePoll'
import ChartPoll from './pages/ChartPoll'

import { isAuthenticated } from './services/auth'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: '/', state: { from: props.location } }} />
      )
    }
  />
)

//TODO: rearrange routes
const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={SignIn} />
      <Route path="/signup" component={SignUp} />
      <Route path="/poll/vote/:id?/" component={VotePoll} />
      <Route path="/polls/vote/list/" component={ListVotesPoll} />
      <Route path="/app" component={MainMenu} />
      <PrivateRoute path="/polls" component={ListPoll} />
      <PrivateRoute path="/edit/poll/:id?/" component={EditPoll} />
      <PrivateRoute path="/create/poll/" component={CreatePoll} />
      <PrivateRoute path="/chart/poll/:id?/" component={ChartPoll} />
      <Route
        path="*"
        component={() => <h1>Page not found! The app is under developments</h1>}
      />
    </Switch>
  </BrowserRouter>
)

export default Routes
