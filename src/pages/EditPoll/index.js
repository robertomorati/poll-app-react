import React from 'react'
import api from '../../services/api'
import MainMenu from '../Menu'
import { withRouter } from 'react-router-dom'
import { Button, Form, FormControl, InputGroup } from 'react-bootstrap'
import Select from 'react-select'
import 'bootstrap/dist/css/bootstrap.min.css'

// TODO: create Component for InputGroup
class EditPoll extends React.Component {
  constructor() {
    super()

    this.state = {
      pk: '',
      title: '',
      description: '',
      shared: '',
      public: '',
      choices: [],
      new_choice: '',
      selectedOption: null,
      removeOption: null,
    }

    this.handleTitleChange = this.handleTitleChange.bind(this)
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
    this.handleSharedChange = this.handleSharedChange.bind(this)
    this.handlePublicChange = this.handlePublicChange.bind(this)
    this.handleNewChoiceChange = this.handleNewChoiceChange.bind(this)
    this.handleAddNewChoiceOnClick = this.handleAddNewChoiceOnClick.bind(this)
    this.handleRemoveChoiceOnChange = this.handleRemoveChoiceOnChange.bind(this)
  }

  async componentDidMount() {
    try {
      const response = await api.get('/polls/' + this.props.match.params['id'])
      const options = []
      if (response.data.choices.length > 0)
        for (let i = 0; i < response.data.choices.length; i++) {
          options.push({
            value: response.data.choices[i]['pk'],
            label: response.data.choices[i]['choice'],
          })
        }

      this.setState({
        pk: response.data.pk,
        title: response.data.title,
        description: response.data.description,
        shared: response.data.shared,
        public: response.data.public,
        choices: options,
      })
    } catch (err) {
      this.props.history.push('/polls')
    }
  }

  //call API to create the Poll
  handleSubmit = async e => {
    e.preventDefault()
    try {
      // TODO: implement Create Choice on API and fix that behavior
      let choices = []
      if (this.state.choices.length > 0)
        for (let i = 0; i < this.state.choices.length; i++) {
          if (!this.state.choices[i]['value'] === '')
            choices.push({
              pk: this.state.choices[i]['value'],
              choice: this.state.choices[i]['label'],
            })
          else
            choices.push({
              choice: this.state.choices[i]['label'],
            })
        }

      await api
        .put('/polls/' + this.state.pk + '/', {
          title: this.state.title,
          description: this.state.description,
          shared: this.state.shared,
          public: this.state.public,
          choices: choices,
        })
        .then(response => {
          this.props.history.push('/polls')
        })
        .catch(err => {})
    } catch (err) {}
  }

  handleCancel = async e => {
    try {
      this.props.history.push('/polls')
    } catch (err) {}
  }

  handleTitleChange(e) {
    this.setState({ title: e.target.value })
  }

  handleDescriptionChange(e) {
    this.setState({ description: e.target.value })
  }

  handlePublicChange(e) {
    this.setState({ public: e.target.checked })
  }

  handleSharedChange(e) {
    this.setState({ shared: e.target.checked })
  }

  handleNewChoiceChange(e) {
    this.setState({ new_choice: e.target.value })
  }

  // remove the selected choice from select
  handleRemoveChoiceOnChange = selectedOption => {
    delete this.state.choices[selectedOption.value]
    let index = this.state.choices.findIndex(
      x => x.label === selectedOption.label
    )
    if (index !== undefined) this.state.choices.splice(index, 1)
  }

  // adds new choice on select
  handleAddNewChoiceOnClick(e) {
    e.preventDefault()
    const option = []
    option.push()

    // the value (pk) is generated in the API
    if (document.getElementById('new_choice').value !== '') {
      this.state.choices.push({
        value: '',
        label: document.getElementById('new_choice').value,
      })
      this.setState({ options: [option] })
      document.getElementById('new_choice').value = ''
    }
  }

  render() {
    return (
      <React.Fragment>
        <MainMenu />
        <div className="container">
          <div className="col-lg-15 col-lg-offset-3">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">Change Poll</h3>
              </div>
              <Form className="container" onSubmit={this.handleFormSubmit}>
                <InputGroup className="mb-3">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="title">Title</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                    placeholder="title of poll"
                    aria-label="title"
                    aria-describedby="title"
                    value={this.state.title}
                    onChange={this.handleTitleChange}
                    type="text"
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="description">
                      Description
                    </InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                    placeholder="description of poll"
                    aria-label="description"
                    aria-describedby="description"
                    value={this.state.description}
                    onChange={this.handleDescriptionChange}
                    type="text"
                  />
                </InputGroup>

                <Form.Group className="mb-3" controlId="choices">
                  <Form.Label>Choices (Votes)</Form.Label>
                  <Select
                    id="selectChoices"
                    placeholder="Select choice to remove..."
                    value=""
                    onChange={this.handleRemoveChoiceOnChange}
                    options={this.state.choices}
                  />
                </Form.Group>
                <div className="input-group">
                  <span className="input-group-btn">
                    <button
                      className="btn btn-link float-left"
                      onClick={this.handleAddNewChoiceOnClick}
                    >
                      Add new choice
                    </button>
                  </span>
                  <FormControl
                    id="new_choice"
                    placeholder="write the new choice"
                    aria-label="new choice"
                    aria-describedby="new choice"
                    defaultValue=""
                    onChange={this.handleNewChoiceChange}
                    type="text"
                  />
                </div>
                <Form.Group controlId="shared">
                  <Form.Check
                    type="checkbox"
                    label="Shared me"
                    checked={this.state.shared}
                    onChange={this.handleSharedChange}
                  />
                </Form.Group>
                <Form.Group controlId="public">
                  <Form.Check
                    type="checkbox"
                    label="Anonymous Vote"
                    checked={this.state.public}
                    onChange={this.handlePublicChange}
                  />
                </Form.Group>
                <Button
                  className="float-right"
                  variant="primary"
                  onClick={this.handleSubmit}
                >
                  Update Changes
                </Button>
                <button
                  className="btn btn-link float-left"
                  onClick={this.handleCancel}
                >
                  Cancel
                </button>
              </Form>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}
export default withRouter(EditPoll)
