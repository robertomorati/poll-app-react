import React from 'react'
import { remove } from 'lodash'
import MainMenu from '../Menu'
import api from '../../services/api'
import { Button } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import { sizePerPageRenderer, customTotal } from '../pagination'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'

class ListPoll extends React.Component {
  constructor(props) {
    super(props)

    this.state = { data: [], total_pages: 1, next: null, previous: null }
    this.handleCreatePoll = this.handleCreatePoll.bind(this)
    this.columns = [
      {
        dataField: 'pk',
        text: 'ID',
        sort: true,
        headerAttrs: { width: 50 },
      },
      {
        dataField: 'title',
        text: 'Title',
        sort: true,
      },
      {
        dataField: 'description',
        text: 'Description',
      },
      {
        dataField: 'shared',
        text: 'Shared',
        sort: true,
        headerAttrs: { width: 200 },
      },
      {
        dataField: 'public',
        text: 'Anonymous Vote',
        sort: true,
        headerAttrs: { width: 200 },
      },
      {
        dataField: 'remove',
        text: 'Remove',
        sort: false,
        formatter: this.formatterRemove,
        headerAttrs: { width: 80 },
        attrs: {
          width: 80,
          className: 'remove',
        },
      },
      {
        dataField: 'update',
        text: 'Update',
        sort: false,
        formatter: this.formatterUpdate,
        headerAttrs: { width: 80 },
        attrs: {
          width: 80,
          className: 'update',
        },
      },
      {
        dataField: 'data_chart',
        text: 'View Chart',
        sort: false,
        formatter: this.formatterViewChart,
        headerAttrs: { width: 80 },
        attrs: {
          width: 80,
          className: 'update',
        },
      },
    ]
  }

  //call deletePoll for remove poll
  formatterRemove = (cell, row, rowIndex, formatExtraData) => {
    return (
      <div style={{ cursor: 'pointer', lineHeight: 'normal' }}>
        <Button
          variant="outline-danger"
          onClick={async props => {
            this.deletePoll({ pk: row.pk })
          }}
        >
          X
        </Button>
      </div>
    )
  }

  //remove poll and update data
  deletePoll = async ({ pk }) => {
    const newData = this.state.data
    try {
      await api
        .delete(`/polls/${pk}/`)
        .then(() => {
          this.props.history.push('/polls')
          remove(newData, n => n.pk === pk)
          this.setState({
            data: newData,
          })
        })
        .catch(err => {})
    } catch (err) {}
  }

  // call update for poll
  formatterUpdate = (cell, row, rowIndex, formatExtraData) => {
    return (
      <div style={{ cursor: 'pointer', lineHeight: 'normal' }}>
        <Button
          variant="outline-primary"
          onClick={async props =>
            this.props.history.push(`/edit/poll/${row.pk}/`)
          }
        >
          X
        </Button>
      </div>
    )
  }

  formatterViewChart = (cell, row, rowIndex, formatExtraData) => {
    return (
      <div style={{ cursor: 'pointer', lineHeight: 'normal' }}>
        <Button
          variant="outline-info"
          onClick={async props =>
            this.props.history.push(`/chart/poll/${row.pk}/`)
          }
        >
          X
        </Button>
      </div>
    )
  }

  async componentDidMount() {
    try {
      // TODO: change pagination to use API pagination. For now page 1 return all data
      const response = await api.get(`/polls/?page=${1}`)
      this.setState({
        data: response.data.results,
        total_pages: response.data.total_pages,
        next: response.data.links.next,
        previous: response.data.links.next,
      })
    } catch (err) {
      this.props.router.push('/app')
    }
  }

  handleCreatePoll(e) {
    this.props.history.push('/create/poll/')
  }

  render() {
    const paginationOptions = {
      paginationSize: 10,
      pageStartIndex: 1,
      firstPageText: 'First',
      prePageText: 'Back',
      nextPageText: 'Next',
      lastPageText: 'Last',
      nextPageTitle: 'First page',
      prePageTitle: 'Pre page',
      firstPageTitle: 'Next page',
      lastPageTitle: 'Last page',
      showTotal: true,
      paginationTotalRenderer: customTotal,
      sizePerPageRenderer: sizePerPageRenderer,
      sizePerPageList: [
        {
          text: '10',
          value: 10,
        },
        {
          text: '15',
          value: 15,
        },
        {
          text: '25',
          value: 25,
        },
        {
          text: 'All',
          value: this.state.data.length,
        },
      ],
    }

    return (
      <React.Fragment>
        <MainMenu />
        <div className="container">
          <div className="col-lg-14 col-lg-offset-3">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">Polls</h3>
              </div>
              <BootstrapTable
                pagination={paginationFactory(paginationOptions)}
                keyField="pk"
                data={this.state.data}
                columns={this.columns}
              />
              <Button variant="primary" onClick={this.handleCreatePoll}>
                Create Poll
              </Button>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}
export default withRouter(ListPoll)
