import React from 'react'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'

export const customTotal = (from, to, size) => (
  <span className="react-bootstrap-table-pagination-total ml-4">
    Showing {from} to {to} of {size} Results
  </span>
)

export const sizePerPageRenderer = ({
  options,
  currSizePerPage,
  onSizePerPageChange,
}) => (
  <React.Fragment>
    <label className="d-inline mr-1">Show</label>
    <select
      name="select"
      id="no-entries"
      className="custom-select custom-select-sm d-inline col-1"
      defaultValue={currSizePerPage}
      onChange={e => onSizePerPageChange(e.target.value)}
    >
      {options.map((option, idx) => (
        <option key={idx}>{option.text}</option>
      ))}
    </select>
    <label className="d-inline ml-1">entries</label>
  </React.Fragment>
)
