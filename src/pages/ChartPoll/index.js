import React from 'react'
import MainMenu from '../Menu'
import api from '../../services/api'
import { Button } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'
import Chart from 'react-google-charts'

class ChartPoll extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      choices: [],
    }
    this.handleComeBack = this.handleComeBack.bind(this)
  }

  handleComeBack = e => {
    try {
      this.props.history.push('/polls')
    } catch (err) {}
  }

  async componentDidMount() {
    try {
      let response = await api.get(
        `/votes/get_data_poll/${this.props.match.params['id']}`
      )

      let data_chart = [
        ['Count', response.data[0].data[0].title, { role: 'style' }],
      ]

      for (let i = 0; i < response.data[0].choices.length; i++) {
        data_chart.push([
          response.data[0].choices[i].choice,
          parseInt(response.data[0].choices[i].votes),
          'color: #76A7FA',
        ])
      }
      this.setState({ choices: data_chart })
    } catch (err) {
      this.props.history.push('/polls')
    }
  }

  render() {
    return (
      <React.Fragment>
        <MainMenu />
        <div className="container">
          <div className="col-lg-15 col-lg-offset-3">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">Chart Poll</h3>
              </div>
              <div className="App">
                <Chart
                  chartType="BarChart"
                  width="100%"
                  height="400px"
                  data={this.state.choices}
                />
                <Button
                  variant="outline-dark"
                  className="float-right"
                  onClick={this.handleComeBack}
                >
                  Back to Polls List
                </Button>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}
export default withRouter(ChartPoll)
