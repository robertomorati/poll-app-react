import React from 'react'
import api from '../../services/api'
import MainMenu from '../Menu'
import { Button } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'
import BootstrapTable from 'react-bootstrap-table-next'
import { sizePerPageRenderer, customTotal } from '../pagination'
import paginationFactory from 'react-bootstrap-table2-paginator'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'

class ListVotesPoll extends React.Component {
  constructor() {
    super()
    this.state = { data: [], total_pages: 1, next: null, previous: null }

    this.columns = [
      {
        dataField: 'pk',
        text: 'ID',
        sort: true,
      },
      {
        dataField: 'title',
        text: 'Title',
        sort: true,
      },
      {
        dataField: 'description',
        text: 'Description',
      },
      {
        dataField: 'update',
        text: 'Click to Vote',
        sort: false,
        formatter: this.formatterVote,
        headerAttrs: { width: 170 },
      },
    ]
  }

  formatterVote = (cell, row, rowIndex, formatExtraData) => {
    return (
      <div
        style={{ textAlign: 'center', cursor: 'pointer', lineHeight: 'normal' }}
      >
        <Button
          variant="outline-primary"
          onClick={async props =>
            this.props.history.push(`/poll/vote/${row.pk}`)
          }
        >
          Vote
        </Button>
      </div>
    )
  }

  async componentDidMount() {
    try {
      const response = await api.get(`/votes/?page=${1}`)

      this.setState({
        data: response.data.results,
        total_pages: response.data.total_pages,
        next: response.data.links.next,
        previous: response.data.links.next,
      })
    } catch (err) {
      this.props.history.push('/app')
    }
  }

  render() {
    const paginationOptions = {
      paginationSize: 10,
      pageStartIndex: 1,
      firstPageText: 'First',
      prePageText: 'Back',
      nextPageText: 'Next',
      lastPageText: 'Last',
      nextPageTitle: 'First page',
      prePageTitle: 'Pre page',
      firstPageTitle: 'Next page',
      lastPageTitle: 'Last page',
      showTotal: true,
      paginationTotalRenderer: customTotal,
      sizePerPageRenderer: sizePerPageRenderer,
      sizePerPageList: [
        {
          text: '10',
          value: 10,
        },
        {
          text: '15',
          value: 15,
        },
        {
          text: '25',
          value: 25,
        },
        {
          text: 'All',
          value: this.state.data.length,
        },
      ],
    }

    return (
      <React.Fragment>
        <MainMenu />
        <div className="container">
          <div className="col-lg-14 col-lg-offset-3">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">Polls to Vote</h3>
              </div>
              <BootstrapTable
                pagination={paginationFactory(paginationOptions)}
                keyField="pk"
                data={this.state.data}
                columns={this.columns}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}
export default withRouter(ListVotesPoll)
