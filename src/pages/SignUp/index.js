import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import api from '../../services/api'

import Logo from '../../assets/logo.svg'

import { Form, Container } from './styles'

class SignUp extends Component {
  state = {
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    error: '',
  }

  handleSignUp = async e => {
    e.preventDefault()
    const { first_name, last_name, email, password } = this.state
    if (!first_name || !last_name || !email || !password) {
      this.setState({ error: 'Fill in all the data to register' })
    } else {
      try {
        await api.post('/users/', { first_name, last_name, email, password })
        this.props.history.push('/')
      } catch (err) {
        this.setState({ error: "The user with this email already exists" })
      }
    }
  }

  render() {
    return (
      <Container>
        <Form onSubmit={this.handleSignUp}>
          <img src={Logo} alt="Poll logo" />
          {this.state.error && <p>{this.state.error}</p>}
          <input
            type="text"
            placeholder="First name"
            onChange={e => this.setState({ first_name: e.target.value })}
          />
          <input
            type="text"
            placeholder="Last name"
            onChange={e => this.setState({ last_name: e.target.value })}
          />
          <input
            type="email"
            placeholder="Email"
            onChange={e => this.setState({ email: e.target.value })}
          />
          <input
            type="password"
            placeholder="Pssword"
            onChange={e => this.setState({ password: e.target.value })}
          />
          <button type="submit">Register</button>
          <hr />
          <Link to="/">Log In</Link>
        </Form>
      </Container>
    )
  }
}

export default withRouter(SignUp)
