import React from 'react'
import { logout } from '../../services/auth'
import { withRouter } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap'
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import 'bootstrap/dist/css/bootstrap.min.css'
import Logo from '../../assets/logo.svg'

class MainMenu extends React.Component {
  handleLogOut = async e => {
    try {
      logout()
      this.props.history.push('/')
    } catch (err) {}
  }

  handleListPolls = async e => {
    try {
      this.props.history.push('/polls')
    } catch (err) {}
  }

  handleListPollsToVote = async e => {
    try {
      this.props.history.push('/polls/vote/list')
    } catch (err) {}
  }

  render() {
    return (
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand>
          <img
            alt=""
            src={Logo}
            width="40"
            height="40"
            className="d-inline-block align-top"
          />{' '}
        </Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Item onClick={this.handleListPolls}>
            <Nav.Link>Polls</Nav.Link>
          </Nav.Item>
          <Nav.Item onClick={this.handleListPollsToVote}>
            <Nav.Link>
              <strong>Vote Here</strong>
            </Nav.Link>
          </Nav.Item>
        </Nav>
        <Form inline onClick={this.handleLogOut}>
          <Button variant="outline-light">LogOut</Button>
        </Form>
      </Navbar>
    )
  }
}
export default withRouter(MainMenu)
