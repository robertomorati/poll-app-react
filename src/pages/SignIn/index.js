import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import Logo from '../../assets/logo.svg'
import api from '../../services/api'
import { login } from '../../services/auth'

import { Form, Container } from './styles'

class SignIn extends Component {
  state = {
    email: '',
    password: '',
    error: '',
  }

  handleSignIn = async e => {
    e.preventDefault()
    const { email, password } = this.state
    if (!email || !password) {
      this.setState({ error: 'Provides email and password to continue' })
    } else {
      try {
        const response = await api.post('/api/token/', { email, password })
        login(response.data.access)
        this.props.history.push('/app')
      } catch (err) {
        this.setState({
          error: 'Check your credentials. T.T',
        })
      }
    }
  }

  render() {
    return (
      <Container>
        <Form onSubmit={this.handleSignIn}>
          <img src={Logo} alt="Poll logo" />
          {this.state.error && <p>{this.state.error}</p>}
          <input
            type="email"
            placeholder="Endereço de e-mail"
            onChange={e => this.setState({ email: e.target.value })}
          />
          <input
            type="password"
            placeholder="Senha"
            onChange={e => this.setState({ password: e.target.value })}
          />
          <button type="submit">Log In</button>
          <hr />
          <Link to="/signup">Sign Up</Link>
          <hr />
          <Link to="/polls/vote/list">Click to Vote in Public Polls</Link>
        </Form>
      </Container>
    )
  }
}

export default withRouter(SignIn)
