import NotificationManager from 'react-notifications'

export default createNotification({ type, message }){
    switch (type) {
      case 'info':
        NotificationManager.info(message)
        break
      case 'success':
        NotificationManager.success('Success message', message)
        break
      case 'warning':
        NotificationManager.warning('Warning message', message, 3000)
        break
      case 'error':
        NotificationManager.error('Error message', 'Click me!', 5000, () => {})
        break

  }
}
