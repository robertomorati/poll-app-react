import React from 'react'
import api from '../../services/api'
import MainMenu from '../Menu'
import { withRouter } from 'react-router-dom'
import { Button, Form } from 'react-bootstrap'
import Select from 'react-select'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'react-notifications/lib/notifications.css'

// TODO: encapsulate components for reuse
// TODO: refactor change vote with cookies or localStorage
class VotePoll extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      pk: '',
      title: '',
      description: '',
      choices: [],
      selectedOption: null,
    }

    this.handleSetSelectedOption = this.handleSetSelectedOption.bind(this)
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption })
  }

  async componentDidMount() {
    try {
      const response = await api.get(`/votes/${this.props.match.params['id']}`)
      const options = []
      if (response.data.choices.length > 0)
        for (let i = 0; i < response.data.choices.length; i++) {
          options.push({
            value: response.data.choices[i]['pk'],
            label: response.data.choices[i]['choice'],
          })
        }

      this.setState({
        pk: response.data.pk,
        title: response.data.title,
        description: response.data.description,
        choices: options,
      })
    } catch (err) {
      this.props.history.push('/polls/vote/list')
    }
  }

  handleFormSubmit = async e => {
    e.preventDefault()
    if (this.state.selectedOption !== null) {
      await api
        .post(`votes/add_vote/${this.state.selectedOption.value}/`, {})
        .then(response => {
          this.props.history.push('/polls/vote/list')
        })
        .catch(err => {})
    }
    try {
    } catch (err) {}
  }

  handleCancel = async e => {
    try {
      this.props.history.push('/polls/vote/list')
    } catch (err) {}
  }

  handleSetSelectedOption = selectedOption => {
    this.setState({ selectedOption: selectedOption })
  }

  render() {
    return (
      <React.Fragment>
        <MainMenu />
        <div className="container">
          <div className="col-lg-8 col-lg-offset-3">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">Cast your vote</h3>
              </div>
              <Form className="container" onSubmit={this.handleFormSubmit}>
                <h4>Question: {this.state.title}</h4>
                <h6>Description: {this.state.description}</h6>
                <Form.Group className="mb-3" controlId="choices">
                  <Form.Label>Choices to Vote</Form.Label>
                  <Select
                    id="selectChoices"
                    placeholder="Select your choice to vote.."
                    value={this.state.selectedOption}
                    onChange={this.handleSetSelectedOption}
                    options={this.state.choices}
                  />
                </Form.Group>
                <Button variant="primary" type="submit">
                  Vote
                </Button>
                <button
                  className="btn btn-link float-left"
                  onClick={this.handleCancel}
                >
                  Cancel
                </button>
              </Form>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}
export default withRouter(VotePoll)
